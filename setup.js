const fs = require('fs');

const createFolderIfMissing = (pathString) => {
  if (!fs.existsSync(pathString)) {
    fs.mkdirSync(pathString);
    console.info(`Created folder: '${pathString}'`);
  }
};

const createFileIfMissing = (pathString, data = '') => {
  if (!fs.existsSync(pathString)) {
    fs.writeFileSync(pathString, data);
    console.info(`Created file: '${pathString}'`);
  }
};

module.exports = function setup() {
  createFolderIfMissing(process.env.SCREENSHOT_FOLDER);
  createFolderIfMissing(process.env.DATA_FOLDER);
  createFileIfMissing(`${process.env.DATA_FOLDER}/${process.env.DATA_FILE}`, JSON.stringify({}));

  const settings = {
    datapath: `./${process.env.DATA_FOLDER}/${process.env.DATA_FILE}`,
    selectors: {
      login: {
        user: '#p_username',
        pass: '#FLEET_AGENT_LOGIN > tbody > tr:nth-child(6) > td:nth-child(2) > input',
        login: '#FLEET_AGENT_LOGIN > tbody > tr:nth-child(7) > td:nth-child(2) > input',
      },
      nav: {
        search: 'body > header > div > div.page-nav > div > ul > li:nth-child(3) > a',
      },
      table: {
        carList: 'tr.clickable',
        cell: 'td',
      },
    },
    cred: {
      user: process.env.USERNAME,
      pass: process.env.PASSWORD,
    },
  };

  return settings;
};
