const puppeteer = require('puppeteer');
const fs = require('fs');
require('dotenv').config();
const slack = require('slack-notify')(process.env.MY_SLACK_WEBHOOK_URL);

const settings = require('./setup')();

let data = JSON.parse(fs.readFileSync(settings.datapath, { encoding: 'utf8' }));

const getCarRowData = async (row) => {
  const description = await row.$eval('td:nth-child(1) > a', node => node.innerText);
  const url = await row.$eval('td:nth-child(1) > a', node => node.href);
  const id = url.split('=')[1];
  const status = await row.$eval('td:nth-child(2)', node => node.innerText);
  const dateAvailable = await row.$eval('td:nth-child(3)', node => node.innerText);
  const bijtellingString = await row.$eval('td:nth-child(4)', node => node.innerText);
  const bijtelling = bijtellingString.split('%')[0];
  const wasSmokedIn = await row.$eval('td:nth-child(5)', node => node.innerText) === 'Ja';
  const carData = {
    id, description, url, status, dateAvailable, bijtelling, wasSmokedIn,
  };

  return carData;
};

const updateCars = (newCars, oldCars) => {
  let fullList = [];
  const alertList = {
    changed: [],
    new: [],
  };
  for (let i = 0; i < newCars.length; i++) {
    let carMatch = false;
    const newCar = newCars[i];
    for (let x = 0; x < oldCars.length; x++) {
      const oldCar = oldCars[x];
      if (newCar.id === oldCar.id) {
        carMatch = true;
        if (newCar.status !== oldCar.status) {
          oldCars.splice(x, 1, newCar);
          alertList.changed.push(newCar);
        }
        break;
      }
    }
    if (carMatch !== true) {
      fullList.push(newCar);
      alertList.new.push(newCar);
    }
  }
  if (oldCars.length > 0) {
    fullList = fullList.concat(...oldCars);
  }
  return {
    fullList,
    alertList,
  };
};

const createDescription = car => `${car.description}\nBijtelling: ${car.bijtelling}%\nStatus: ${car.status}\nBeschikbaar per: ${car.dateAvailable}\nGerookt: ${car.wasSmokedIn ? 'Ja' : 'Nee'}`;

const sendUpdates = (alertList) => {
  alertList.new.forEach(car => slack.alert(`\n\n---------------\n\n:new: New car:\n\n${createDescription(car)}`));
  alertList.changed.forEach(car => slack.alert(`\n\n---------------\n\nStatus changed to ${car.status} for:\n\n${createDescription(car)}`));
};

async function run() {
  const browser = await puppeteer.launch({
    // headless: false,
    args: ['--no-sandbox'],
  });
  const page = await browser.newPage();
  page.setViewport({
    width: 1024,
    height: 786,
  });
  try {
    await page.goto('https://fleetagent.alphabet.com');
    await page.screenshot({ path: 'screenshots/fleetagent.png' });

    // Login
    await page.waitForSelector(settings.selectors.login.user);
    await page.click(settings.selectors.login.user);
    await page.keyboard.type(settings.cred.user);

    await page.waitForSelector(settings.selectors.login.pass);
    await page.click(settings.selectors.login.pass);
    await page.keyboard.type(settings.cred.pass);

    await page.waitForSelector(settings.selectors.login.login);
    await page.click(settings.selectors.login.login);

    await page.waitForSelector(settings.selectors.nav.search);
    await page.click(settings.selectors.nav.search);

    await page.waitForSelector(settings.selectors.table.carList);
    const carRows = await page.$$(settings.selectors.table.carList);
    const newCars = [];
    for (let i = 0; i < carRows.length; i += 1) {
      newCars.push(await getCarRowData(carRows[i]));
    }
    const updatedCars = updateCars(newCars, data);
    sendUpdates(updatedCars.alertList);
    data = updatedCars.fullList;
    // data = newCars;
  } catch (e) {
    console.error('ERROR:', e);
  }
  browser.close();

  fs.writeFileSync(settings.datapath, JSON.stringify(data));
}

run();
